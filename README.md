# System Requirements

This requires the [International Components for Unicode][libicu] tools
and development headers installed. This is required by the
[Bindings to the ICU library][text-icu] package. For APT-based
systems, the system package is [libicu-dev][libicu-dev].

# NFC-Normalized Text

This provides a `newtype` wrapper around `Data.Text` for ensuring all
values are [NFC Normalized][nfc]. NFC Normalization is "Normalization
Form C (NFC): Canonical Decomposition, followed by Canonical
Composition". [NFC][norm-forms] is also
[recommended by the W3C][w3c-norm].

This also provides multiple Prisms for convertingn between strict and
lazy text, UTF-8 strings, plus upper and lower case. Currently the
locale is hardcoded to `en_US.UTF8`, but at some point this could be
an option.

```haskell
# NFC Normalization Overview

_(Excerpted from [Unicode TR 15 Section 1.3][nfc-description])_

To transform a Unicode string into a given Unicode Normalization Form,
the first step is to fully decompose the string. The decomposition
process makes use of the
[`Decomposition_Mapping`][DecompositionMapping] property values
defined in [`UnicodeData.txt`][UnicodeData] file.  There are also
special rules to fully decompose Hangul syllables. Full decomposition
involves recursive application of the
[`Decomposition_Mapping`][DecompositionMapping] values, because in
some cases a complex composite character may have a
[`Decomposition_Mapping`][DecompositionMapping] into a sequence of
characters, one of which may also have its own non-trivial
[`Decomposition_Mapping`][DecompositionMapping] value.

The type of full decomposition chosen depends on which Unicode
Normalization Form is involved. For [NFC][norm-forms] or
[NFD][norm-forms], one does a full canonical decomposition, which
makes use of only canonical
[`Decomposition_Mapping`][DecompositionMapping] values. For
[NFKC][norm-forms] or [NFKD][norm-forms], one does a full
compatibility decomposition, which makes use of canonical and
compatibility [`Decomposition_Mapping`][DecompositionMapping] values.

Once a string has been fully decomposed, any sequences of combining
marks that it contains are put into a well-defined order. This
rearrangement of combining marks is done according to a subpart of the
Unicode Normalization Algorithm known as the Canonical Ordering
Algorithm. That algorithm sorts sequences of combining marks based on
the value of their [`Canonical_Combining_Class`][ccc] (ccc) property,
whose values are also defined in
[`UnicodeData.txt`][UnicodeData]. Most characters (including all
non-combining marks) have a [`Canonical_Combining_Class`][ccc] value
of zero, and are unaffected by the Canonical Ordering Algorithm. Such
characters are referred to by a special term, starter. Only the subset
of combining marks which have non-zero
[`Canonical_Combining_Class`][ccc] property values are subject to
potential reordering by the Canonical Ordering Algorithm. Those
characters are called non-starters.

At this point, if one is transforming a Unicode string to [NFD][norm-forms] or [NFKD][norm-forms],
the process is complete. However, one additional step is needed to
transform the string to [NFC][norm-forms] or [NFKC][norm-forms]: recomposition. The fully
decomposed and canonically ordered string is processed by another
subpart of the Unicode Normalization Algorithm known as the Canonical
Composition Algorithm. That process logically starts at the front of
the string and systematically checks it for pairs of characters which
meet certain criteria and for which there is a canonically equivalent
composite character in the standard. Each appropriate pair of
characters which meet the criteria is replaced by the composite
character, until the string contains no further such pairs. This
transforms the fully decomposed string into its most fully composed
but still canonically equivalent sequence.

# References

* [W3C NFC Recommendation][w3c-norm]
* [Normalization Forms][norm-forms]
* [`UnicodeData.txt`][UnicodeData]
* [Normalization Overview][norm-forms]
* [Canonical Combining Class][ccc]

[w3c-norm]: https://www.w3.org/TR/2012/WD-charmod-norm-20120501/#sec-ChoiceNFC "W3C NFC Recommendation"

[norm-forms]: http://www.unicode.org/reports/tr15/#Norm_Forms "Normalization Forms"

[UnicodeData]: http://www.unicode.org/Public/UNIDATA/UnicodeData.txt

[nfc-description]: http://www.unicode.org/reports/tr15/#Description_Norm "Normalization Overview"

[DecompositionMapping]: http://www.unicode.org/reports/tr44/#Decomposition_Mapping "Decomposition Mapping"

[ccc]: http://www.unicode.org/reports/tr44/#Canonical_Combining_Class_Values "Canonical Combining Class"

[libicu]: http://site.icu-project.org/ "International Components for Unicode"

[text-icu]: http://hackage.haskell.org/package/text-icu "Bindings to the ICU library"

[libicu-dev]: https://packages.debian.org/sid/libicu-dev "Development files for International Components for Unicode"
